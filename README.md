# Project title

A simple CRUD in rust


### 📋 Prerequisites

```
rust installed on your machine
```

### 🔧 Installation


after cloning the repo run:

```
cargo run
```
### 🔩 using CRUD

after compiling you open postman or insomnia

```

you have to put an authorization in the header:

Authorization  = Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==

which for now is a simple base64 🤓🤓🤓🤓

//////////////////////////////////////////////////////////

create route = POST: 127.0.0.1:8000/rustaceans/ body(
    {
        "name": "name1", 
        "email": "email@email.com"
    }
)

////////////////////////////////////////////////////

get route: 127.0.0.1:8000/rustaceans/    --> no body

////////////////////////////////////////////////////

get route: 127.0.0.1:8000/rustaceans/       -->  id  no body

////////////////////////////////////////////////////

update route: 127.0.0.1:8000/rustaceans/  -->  id body(
    json =
    
    {
        "id": 1,	
        "name": "name1", 
        "email": "email@email.com"
    }
)

////////////////////////////////////////////////////

DLEETE route: 127.0.0.1:8000/rustaceans/ --> id body(
    json =

    {
	    "id": 1
    }
)


```


## 🎁 Expressions of gratitude

* Tell others about this project 📢
* Invite someone from the team for a beer 🍺
*Thank you publicly 🤓.
* etc.
